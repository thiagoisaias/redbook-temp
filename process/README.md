# Process
[Home](../README.md)

## Git
<!-- Colocar questões referentes a estilo no style guide -->

Siga as recomendações do style guide. (TODO: colocar link para o style guide)

**Mantendo o repositório**
  - Um workflow baseado em feature branches deve ser seguido por padrão. A equipe deve discutir e entrar em um consenso.
  - Cuidado com arquivos que são específicos do ambiente de desenvolvimento local. Adicione-os no `.gitignore`.
  - Deletar as branchs (local e remote) após o merge.

**Resolvendo um Ticket/Issue**

Escreva as alterações (feature, bug ou refactoring) em uma nova branch. 
> `git checkout master` <br/>
> `git checkout -b <branch-name>`

Se necessário, pegue as alterações da master (ou develop, dependendo do workflow escolhido) através de um `merge`.
> `git checkout <branch-name>` <br />
> `git merge master`

Dê um push das suas alterações
> `git checkout <branch-name>` <br />
> `git push origin HEAD`


**Review**

Todas as branches precisam de revisão antes do merge na master. <br />
Antes de abrir o MR organize o histórico commits de sua branch usando o rebase -i. <br />
  > `git checkout <baranch-name>` <br />
  > `git rebase -i HEAD~5` (exibe os 5 últimos commits)
  1. Dê squash em commits pequenos, como alterações de 1 linhas de código, por exemplo.
  2. Melhore as mensagens de alguns commits, se necessário.
  3. Altere a ordem dos commits, se necessário.

Após organizar a sua branch localmente, envie as alterações para a branch remota. Note que será preciso utilizar `--force` pois a branch local e a remota divergem após esse rebase.
> `git push origin HEAD --force`


**Merge**

Aguarde o seu MR ser aceito por pelo menos 1 outro programador. <br />
Aguarde os Jobs da CI terminarem (com status positivo). <br />
Faça o merge das alterações através do GitLab. Evite fazer o merge através do terminal e **não** coloque as alterações realizadas na master usando o rebase.

Delete a branch do git local e do remote:

>`git push origin --delete <branch-name>` <br />
>`git branch --delete <branch-name>`


## Tickets/Issues Management

**Trello**
- Um novo Team deve ser criado para cada projeto.
- Enviar um bulk invite para toda a equipe interna.
- Novos cards são criados nos boards Backlog e Engineering.
- O board Development representa o estado da sprint atual e recebe os cards dos boards Backlog e Engineering.
- O card é criado apenas com o título, mas o seu nível de detalhamento deve aumentar conforme ele avança nos boards.
- O seguinte template deve ser usado. No entanto, a equipe tem a liberdade de adicionar novas listas de acordo com a necessidade do projeto.

> **Backlog**
>  * _Discovery:_ Lista com novas features necessárias que foram descobertas durante o processo de desenvolvimento.
>  * _Inbox:_ Lista com as features que foram listadas e estimadas durante o planejamento.

> **Engineering**
>  * _Bugs:_ Adicionar labels de acordo com a severidade do problema.
>  * _Refactoring:_

> **Development**
>  * _Next Up:_ Essa lista representa a sprint atual. Ao mover um card para essa lista é necessário
>  * _In Progress_
>  * _In Review:_ Ao mover um card para essa lista, adicionar o link para o Merge Request.
>  * _Done_

Caso o projeto tenha mais de um programador trabalhando paralelamente, adicione prefixos nos boards do template padrão e escolha uma cor para cada prefixo. Dessa maneira a transferência de cards entre os Boards fica simples. 
  * Mobile App - Backlog / Mobile App - Engineering / Mobile App - Development
  * API - Backlog / API - Engineering / API - Development


## Code Review

**Geral**
- Lembre-se que a maioria das decisões de programação são opiniões.
- Faça boas perguntas e não faça demandas ("Você não acha melhor mudar o nome dessa variável `xyz123`?").
- Boas perguntas evitam julgamento e suposições sobre a perspectiva do autor.
- Se você tem dúvidas, não hesite em perguntar.
- Evite pronomes possessivos ("meu", "seu").
- Evite usar termos sobre a qualidade intelectual das pessoas ("burro", "gênio"). Assuma que todos são inteligentes e bem intencionados. 
- Evite sarcasmo.
- Seja claro! Não é fácil identificar intenções online.
- Seja humilde e educado ("Obrigado pela sugestão", "Beleza, vou dar uma olhada").
- O Merge Request é uma excelente oportunidade para aprender e ensinar.


**O seu código será revisado?**

- Explique o propósito daquele código. Coloque na descrição do MR as informações necessárias para que outro programador consiga revisar o seu código.
- Coloque uma label critical no gerenciador de issues caso o MR seja urgente.
- Não leve nada para o lado pessoal. A revisão que está sendo feita é do código e não do programador.
- Sempre assuma que o revisor teve a melhor das intenções.
- Nem todas as alterações necessárias serão simples. Se necessário, crie novas issues para resolver futuramente.
- Coloque o link do MR na ferramenta de gerenciamento de issues (Trello ou Clickup).
- Tente entender o ponto de vista do revisor.
- Tente responder todos os comentários.
- Espere todos os jobs da CI terminarem para fazer o merge das alterações aprovadas.


**Você revisará o código de outro programador?**

Primeiro entenda o propósito do MR (correção de bug, alteração de estilo, refatorando código, nova funcionalidade) e então: 

- Tente entender o ponto de vista do autor.
- Assine o MR com um 👍 ou algum comentário como: "Tudo ok!", "Revisado".
- Comente sobre violações aos guias de estilo. Coloque o link e explique qual foi o problema.