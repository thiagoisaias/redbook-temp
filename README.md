# Rocketbook

Esse documento foi baseado no _Handbook_ do Gitlab e no _Playbook_ da Toughtbot.

Sinta-se livre para adicionar sugestões através de um merge request.


- [Process](./process/README.md)
- [Production Review](./production-review/README.md)
- [Project Setup](./project-setup/README.md)
- [Style Guides](./style-guides/README.md)
