## Project Setup
[Home](../README.md)

**Accounts**

- Criar um email para o projeto. Anotar as senhas de acesso e adicioná-las na pasta do drive.

**Todas as contas serão criadas usando esse email.**
- Adicione qualquer conta auxiliar (Red ou Desenvolvedor) como colaborador, se necessário (caso do Heroku, por exemplo).

**GitLab**

- Todos os projetos devem estar no GitLab como repositórios privados.
- Para evitar ambiguidade entre os repositórios, o seguinte padrão deve ser usado:
**project-api / project-app / project-web / project-adm**
Onde *project* é um nome pequeno. Qualquer explicação extra pode ser colocada na descrição.
- O Readme deve conter informações relevantes: 
  1. Tecnologias utilizadas
  2. Setup do projeto
  3. Dependências
- Criar os remotes de *staging* e *production* com os links de seus respectivos apps no Heroku.

**Gerenciamento de Issues**

Criar um Team no Trello seguindo template:
* Backlog
  1. Discovery
  2. Inbox
* Engineering
  1. Bugs
  2. Refactoring
* Development
  1. Next Up
  2. In Progress
  3. In Review
  4. Done

**Heroku**

- Crie uma pipeline com o nome do projeto (usar a conta do projeto) e adicione os aplicativos de staging e production
*project-staging*
*project*

**TestFlight & Android APKs**

- todo