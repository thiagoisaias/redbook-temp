# Git
[Guides](../README.md)

**Best Practices**

- *Commit Often* - Não acumule alterações.  Faça o commit constantemente.
- *Commit Related Changes* - Alterações não relacionadas devem gerar mais de um commit.
- *Don’t Commit Half-Done Work* - Divida a feature em blocos e faça o commit apenas de código que esteja funcionando.
- *Test Before You Commit* - 👀

**Types**

Um prefixo com algum _type_ pode ser usado no nome de uma branch ou no _summary_ das mensagens de commit.

> Feat: A new feature. <br/>
> Fix: A bug fix. <br/>
> Docs: Changes to documentation. <br/>
> Style: Formatting, missing semi colons, etc. <br/>
> Refactor: Refactoring existing code. <br/>
> Test: Adding or refactoring tests. <br/>
> Chore: Updating configuration files. <br/>


**Commit Messages**
- Não escreva a mensagem do commit usando a flag -m. Ela passa a sensação de que toda a mensagem deve estar contida em uma única linha.
  - Execute o seguinte comando, se necessário: `git config --global core.editor /usr/bin/vim`

Utilize o seguinte padrão para as suas mensagens.

>Capitalized, short (50 chars or less) summary
>
>More detailed explanatory text, **if necessary**.  Wrap it to about 72
>characters or so.  In some contexts, the first line is treated as the
>subject of an email and the rest of the text as the body.  The blank
>line separating the summary from the body is critical (unless you omit
>the body entirely); tools like rebase can get confused if you run the
>two together.
>
>Write your commit message in the imperative: "Fix bug" and not "Fixed bug"
>or "Fixes bug."  This convention matches up with commit messages generated
>by commands like git merge and git revert.
>
>Further paragraphs come after blank lines.
>
>- Bullet points are okay, too
>
>- Typically a hyphen or asterisk is used for the bullet, followed by a
>  single space, with blank lines in between, but conventions vary here
>
> Reference issues/merge requests/snippets here #id #!id $id  